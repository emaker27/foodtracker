//
//  HealthStoreConstant.swift
//  FoodTracker
//
//  Created by Elton Nix on 7/19/15.
//  Copyright (c) 2015 Rare Agenda. All rights reserved.
//

import Foundation
import HealthKit

class HealthStoreConstant {
    let healthStore: HKHealthStore? = HKHealthStore()
}